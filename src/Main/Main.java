package Main;

public class Main {
    public static void main(String[] args) {
        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);
        Employee billEnd = new Employee("Bill", "End", 999);

        SimpleHashTable ht = new SimpleHashTable();
        ht.put("Jones", janeJones); //position 5
        ht.put("Doe", johnDoe);             //menurut panjang karakter
        ht.put("Wilson", mikeWilson);
        ht.put("Smith", marySmith);

        ht.printHashTable();
        System.out.println("============================================");

        System.out.println("Retrive key Wilson: " + ht.get("Wilson"));
        System.out.println("Retrive key Smith: " + ht.get("Smith"));

        System.out.println("============================================");

        ht.remove("Wilson");
        ht.remove("Jones");


        ht.printHashTable();
    }
}

