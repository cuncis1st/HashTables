package Chaining;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class ChainedHashTable {
    private LinkedList<StoredEmployee>[] hashTable;

    public ChainedHashTable() {
        hashTable = new LinkedList[10];
        for (int i = 0; i < hashTable.length; i++) {
            hashTable[i] = new LinkedList<>();
        }
    }

    public void put(String key, Employee employee) {
        int hashedKey = hashKey(key);
        hashTable[hashedKey].add(new StoredEmployee(key, employee));
    }

    public Employee get(String key) {
        int hashedKey = hashKey(key);
        ListIterator<StoredEmployee> it = hashTable[hashedKey].listIterator();
        StoredEmployee employee = null;
        while (it.hasNext()) {
            employee = it.next();
            if (employee.key.equals(key)) {
                return employee.employee;
            }
        }
        return null;
    }

    public Employee remove(String key) {
        int hashedKey = hashKey(key);
        ListIterator<StoredEmployee> it = hashTable[hashedKey].listIterator();
        StoredEmployee employee = null;
        int index = -1;
        while (it.hasNext()) {
            employee = it.next();
            index++;
            if (employee.key.equals(key)){
                break;
            }
        }

        if (employee == null) {
            return null;
        } else {
            hashTable[hashedKey].remove(index);
            return employee.employee;
        }
    }

    private int hashKey(String key) {
        return key.length() % hashTable.length;
    }

    public void printHashTable() {
        for (int i = 0; i < hashTable.length; i++) {
            if (hashTable[i].isEmpty()) {
                System.out.println("Position " + i + ": Empty");
            } else {
                System.out.print("Position " + i + ": ");
                ListIterator<StoredEmployee> it = hashTable[i].listIterator();
                while (it.hasNext()) {
                    System.out.print(it.next().employee);
                    System.out.print(" -> ");
                }
                System.out.println("null");
            }
        }
    }


}
