package Map;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Main {
    public static void main(String[] args) {
        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);

        Map<String, Employee> hashMap = new HashMap<String, Employee>();
        hashMap.put("Jones", janeJones);
        hashMap.put("Doe", johnDoe);
        hashMap.put("Smith", marySmith);
        hashMap.put("Wilson", mikeWilson);

        Employee employee = hashMap.putIfAbsent("Smith", null);
        System.out.println(employee);
        System.out.println(hashMap.get("Jane"));    //wrong! because this just last name
//        System.out.println(hashMap.remove("Doe"));
        System.out.println("================================");

        System.out.println(hashMap.containsKey("Doe"));
        System.out.println(hashMap.containsValue(janeJones));   //more faster
        System.out.println("================================");

        hashMap.forEach((k, v) ->
                System.out.println("Key: " + k + ", Value: " + v)) ;


        for (String data: hashMap.keySet()) {   //gets the keys
            System.out.println(data);
        }

        System.out.println();

        for (Employee data: hashMap.values()) {     //get the values
            System.out.println(data);
        }

        System.out.println();

        for (Entry<String, Employee> data: hashMap.entrySet()) {     //which has both
            System.out.println(data);
        }


    }
}
